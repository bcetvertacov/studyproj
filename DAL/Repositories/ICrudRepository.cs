﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    interface ICrudRepository<T> where T : class
    {
        IEnumerable<T> findAll();
        T GetById(object id);
        void delete(object id);
        void insert<T>(T obj);
        void Save();
    }
}
