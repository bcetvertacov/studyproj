﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using CONTEXT;
using System.Linq;

namespace DAL.Repositories
{
    public abstract class AbstractCrudRepository<T> : ICrudRepository<T> where T : class
    {
        private MyDBContext _myDbContext;
        private DbSet<T> table;

        public AbstractCrudRepository() => this._myDbContext = new MyDBContext();

        public virtual void delete(object id)
        {
            var obj = table.Find(id);
            _myDbContext.Remove(obj);
        }

        public IEnumerable<T> findAll()
        {
            return table.ToList();
        }

        public T GetById(object id)
        {
            return table.Find(id);
        }

        public void insert<T>(T obj)
        {
            _myDbContext.Add(obj);
        }

        public void Save()
        {
            _myDbContext.SaveChanges();
        }
    }
}
