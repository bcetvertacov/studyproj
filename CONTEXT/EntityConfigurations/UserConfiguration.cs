﻿using DATA.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DAL.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasKey(u => u.Id);

            builder
                .Property(u => u.Name)
                .HasMaxLength(20)
                .IsRequired();

            builder
                .Property(u => u.Surname)
                .HasMaxLength(30)
                .IsRequired();
        }
    }
}
