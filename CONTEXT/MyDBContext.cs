﻿using DAL.EntityConfigurations;
using DATA.Entities;
using Microsoft.EntityFrameworkCore;

namespace CONTEXT
{
    public class MyDBContext : DbContext
    {
        public DbSet<User> Students { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=MyStudyDB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
        }


    }
}
