﻿using DATA.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DATA.Proxies
{
    public class UserProxy
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public static UserProxy Convert(User usr) => new UserProxy()
        {
            Id = usr.Id,
            Name = usr.Name
        };
    }
}
