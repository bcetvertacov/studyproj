﻿using DAL.RepositoryIml;
using DATA.Proxies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BUSINESS.Services
{
    public class UserService
    {
        private UserRepositoryImpl _userServiceRepositroy;

        public IEnumerable<UserProxy> getAll()
        {
            return _userServiceRepositroy.findAll().Select(u => UserProxy.Convert(u));
        }
    }
}
