﻿using BUSINESS.Services;
using DATA.Proxies;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Controllers
{
    [ApiController]
    public class UserController : ControllerBase
    {
        private UserService _userSerice;

        [HttpGet]
        public IEnumerable<UserProxy> users()
        {
            var usersList = _userSerice.getAll();
            return usersList;
        }
    }
}
